const fs = require('fs')
const os = require('os')
const path = require('path')

const parse = (src) => {
  const result = {}
  src.toString().split('\n')
    .forEach(line => {
      const keyVal = line.match(/^\s*([\w.-]+)\s*=\s*(.*)?\s*$/)
      if (keyVal !== null) {
        const key = keyVal[1]
        let val = keyVal[2] || ''
        const len = val ? val.length : 0
        if (len > 0 && val.charAt(0) === '"' && val.charAt(len - 1) === '"') {
          val = val.replace(/\\n/gm, '\n')
        }
        val = val.replace(/(^['"]|['"]$)/g, '').trim()
        result[key] = val
      }
    })
  return result
}

const walk = (dir, file, done) => {
  const list = fs.readdirSync(dir)
  const { root } = path.parse(process.cwd())
  list.forEach((f) => {
    const filePath = path.resolve(dir, f)
    if (f === file) {
      return done(f)
    }
    if (dir !== os.homedir()) {
      return walk(path.resolve(dir, '..'), file, done)
    }
  })
} 

module.exports = (filename = '.env') => {
  const encoding = 'utf8'
  let p = path.resolve('')
  try {
    walk(p, filename, (file) => {
      const parsed = parse(
        fs.readFileSync(path.resolve(file), { encoding })
      )

      Object.keys(parsed).forEach(key => {
        if (!process.env.hasOwnProperty(key)) {
          process.env[key] = parsed[key]
        }
      })
      return { parsed }
    })
  } catch (err) {
    console.error(err)
  }
}
